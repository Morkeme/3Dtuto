var renderer, WIDTH, HEIGHT, c, camera, scene, 
	cubeGeoJambe, cubeGeoCorps, cubeGeoTete, cubeMat, 
	cubeTete, cubeCorps, cubeJambe, axisHelper, 
	urlCorps, urlTete, urlJambes, texture,
	CorpsMaterial, JambeMaterial , TeteMaterial,
	textureCorps, textureTete, textureJambe,
	urlBrasG, textureBrasG, BrasGMaterial, cubeGeoBrasG,
	cubeBrasG, urlBrasD, textureBrasD, cubeGeoBrasD, 
	cubeBrasD, BrasDMaterial  ;

var texCorps = function(){
	
		textureCorps = texCorps;
		textureCorps.needsUpdate = true;
		
}

var texTete = function(){
	
		textureTete = texTete;
		textureTete.needsUpdate = true;
		
}

var texJambe = function(){
	
		textureJambe = texJambe;
		textureJambe.needsUpdate = true;
		
}

var texBrasG = function(){
	
		textureBrasG = texBrasG;
		textureBrasG.needsUpdate = true;
		
}

var texBrasD = function(){
	
		textureBrasD = texBrasD;
		textureBrasD.needsUpdate = true;
		
}

function init()
{
	HEIGHT = 360 ;
	WIDTH = 640 ;
	urlCorps = "assets/corps.png";
	urlTete = "assets/tete.png";
	urlJambes = "assets/jambe.png";
	urlBrasG = "assets/brasG.png";
	urlBrasD = "assets/brasD.png";
	//url = "assets/02.jpg";
	
	renderer = new THREE.WebGLRenderer();
	renderer.setSize(WIDTH,HEIGHT);
	
	c = document.getElementById("gameCanvas");
	c.appendChild(renderer.domElement);
	
	camera = new THREE.PerspectiveCamera(45, WIDTH/HEIGHT, 1, 1000);
	scene = new THREE.Scene();
	axisHelper = new THREE.AxisHelper();
	
	cubeMat = new THREE.MeshNormalMaterial();
	textureCorps =  new THREE.TextureLoader().load(urlCorps, texCorps)
	CorpsMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff, map: textureCorps } );
	
	textureTete =  new THREE.TextureLoader().load(urlTete, texTete)
	TeteMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff, map: textureTete } );
	
	textureJambe =  new THREE.TextureLoader().load(urlJambes, texJambe)
	JambeMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff, map: textureJambe } );
	
	textureBrasG =  new THREE.TextureLoader().load(urlBrasG, texBrasG)
	BrasGMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff, map: textureBrasG } );
	
	textureBrasD =  new THREE.TextureLoader().load(urlBrasD, texBrasD)
	BrasDMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff, map: textureBrasD } );
	
	cubeGeoJambe = new THREE.CubeGeometry( 0.5, 0.8, 0.2);
	cubeGeoCorps = new THREE.CubeGeometry( 0.5, 1, 0.2);
	cubeGeoTete = new THREE.CubeGeometry( 0.5, 0.4, 0.35);
	cubeGeoBrasG = new THREE.CubeGeometry( 0.2, 0.9, 0.35);
	cubeGeoBrasD = new THREE.CubeGeometry( 0.2, 0.9, 0.35);
	
	
	
	cubeJambe = new THREE.Mesh(cubeGeoJambe,JambeMaterial);
	cubeCorps = new THREE.Mesh(cubeGeoCorps,CorpsMaterial);
	cubeTete = new THREE.Mesh(cubeGeoTete,TeteMaterial);
	cubeBrasG = new THREE.Mesh(cubeGeoBrasG,BrasGMaterial);
	cubeBrasD = new THREE.Mesh(cubeGeoBrasD,BrasDMaterial);

	scene.add(camera);
	scene.add(axisHelper);
	scene.add(cubeJambe);
	scene.add(cubeCorps);
	scene.add(cubeTete);
	scene.add(cubeBrasG);
	scene.add(cubeBrasD);
	cubeTete.position.y = 0.7;
	cubeCorps.position.y = 0;
	cubeBrasG.position.y = -0.1
	cubeBrasG.position.x = 0.32;
	cubeBrasD.position.y = -0.1
	cubeBrasD.position.x = -0.32;
	cubeJambe.position.y = -0.9;
	//scene.add(cubeCorps);
	//scene.add(cubeTete);
	camera.position.z = 4 ;
	render();
}


function punchLeft(){

	
}

function render()
{
	renderer.render(scene, camera);
	//cubeJambe.rotation.x += 0.01;
	//cubeJambe.rotation.y += 0.01;
	//cubeCorps.rotation.x += 0.01;
	//cubeCorps.rotation.y += 0.01;
	//cubeTete.rotation.x += 0.01;
	//cubeTete.rotation.y += 0.01;
	
	
	
	requestAnimationFrame(render);
}
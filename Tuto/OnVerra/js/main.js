var renderer, WIDTH, HEIGHT, c, camera, scene, controls, cubeGeo, cubeMat, 
	cube, axisHelper, urlGrass,GrassMaterial,textureGrass, planeGeo, planeMat, PlaneTexture, Plane ;


var texGrass = function(){
	
		textureGrass = texGrass;
		textureGrass.needsUpdate = true;
		
}


function init()
{
	HEIGHT = 360 ;
	WIDTH = 640 ;
	urlGrass = "assets/03.jpg";
	renderer = new THREE.WebGLRenderer();
	renderer.setSize(WIDTH,HEIGHT);
	
	c = document.getElementById("gameCanvas");
	c.appendChild(renderer.domElement);
	
	camera = new THREE.PerspectiveCamera(45, WIDTH/HEIGHT, 1, 1000);
	scene = new THREE.Scene();
	axisHelper = new THREE.AxisHelper();
	
	//Controle de la caméra avec la souris
	controls = new THREE.OrbitControls( camera, renderer.domElement );
	controls.enableDamping = true;
	controls.dampingFactor = 0.25;
	controls.enableZoom = false;
	
	cubeMat = new THREE.MeshNormalMaterial();
	textureGrass =  new THREE.TextureLoader().load(urlGrass, texGrass)
	GrassMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff, map: textureGrass } );	
	cubeGeo = new THREE.CubeGeometry(20, 20, 20);
	cube = new THREE.Mesh(cubeGeo,cubeMat);
	
	planeMat = new THREE.MeshNormalMaterial();
	planeGeo = new THREE.PlaneBufferGeometry( 2000, 2000 );
	Plane = new THREE.Mesh(planeGeo,GrassMaterial);

	scene.add(camera);
	scene.add(axisHelper);
	scene.add(cube);
	scene.add(Plane);
	
	camera.position.z = 90 ;
	camera.position.y = 50;
	//camera.position.x = 5;
	
	Plane.rotation.x = -Math.PI / 2;
	Plane.position.y = -30
	Plane.position.z = -30;
	Plane.rotation.y = -Math.PI / 204;
	
	cube.position.y = -20;
	
	camera.lookAt( scene.position );
	render();
}


function render()
{
	renderer.render(scene, camera);
	controls.update(); // required if controls.enableDamping = true, or if controls.autoRotate = true
	//Plane.rotation.y += 0.01;
	requestAnimationFrame(render);
}